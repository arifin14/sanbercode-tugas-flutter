class Segitiga {
  late double sisi;
  late double alas;
  late double tinggi;

  keliling(sisi) {
    return sisi * 3;
  }

  double luas(alas, tinggi) {
    return 0.5 * (alas * tinggi);
  }
}

import 'dart:math';

class Lingkaran {
  double phi = 3.14;

  double Luas(_r) {
    return phi * pow(_r, 2);
  }
}

class Persegi {
  late double panjang;
  late double lebar;
  late double sisi;

  keliling(sisi) {
    return sisi * 4;
  }

  luas(panjang, lebar) {
    return panjang * lebar;
  }
}

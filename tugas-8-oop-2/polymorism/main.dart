import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  Segitiga segitiga = new Segitiga();
  Lingkaran lingkaran = new Lingkaran();
  Persegi persegi = new Persegi();

  print("Luas Segitiga : ${segitiga.luas(20, 10)}");
  print("Keliling Segitiga : ${segitiga.keliling(20)}");

  print("Luas Lingkaran : ${lingkaran.Luas(10)}");

  print("Luas Persegi : ${persegi.luas(20, 10)}");
  print("Keliling Persegi : ${persegi.keliling(20)}");
}

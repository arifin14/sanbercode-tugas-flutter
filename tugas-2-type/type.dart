
import 'dart:io';

import 'materi.dart';

void main(args) {
  menginputText(args);
  aksesKarakter();
  tipeData();
  aksesKarakter();
  number(args);
  operatorData();
  
}

void menginputText(List<String>argas) {
  print("masukan nama lengkap");
  String inputText = stdin.readLineSync()!;
  print("Nama Lengkap:  ${inputText}");
}

void tipeData() {
  var nama = "Misbahul Arifin"; // String
  var umur = 20; //  number 
  var sudahMenikah = false; // boolean
  print(nama);
  print(umur);
  print(sudahMenikah);
}

void aksesKarakter() {
  var nama = "Misbahul Arifin";
  print(nama[0]); // String
  print(nama[1]); // String
  print(nama[2]); // String
  print(nama[3]); // String
}

void number(List<String> args){
  // deklarasi type data number 
  int num01 = 10;
  double num02 = 10.5;
  print(num01);
  print(num02);
  // mengubah bentuk string ke number
  print(num.parse('10'));
  print(num.parse('10.5'));

  // mengubah bentuk dari numer ke string
  String f = "$num01";
  print('convert '+ f);  
}

void operator() {
  var angka = 100;
  print(angka == 100); // true
  print(angka == 20); // false

  // Not Equal ( != )
  var sifat = "rajin";
  print(sifat != "malas"); // true
  print(sifat != "bandel"); //true 

  // Strict Equal ( == ) 
  var strickAngka = 8;
  print(strickAngka == "8"); // false
  print(strickAngka == "8"); // false
  print(strickAngka == 8); // true


  // Strict Not Equal ( != ) 
  var notEqualAngka = 11;
  print(notEqualAngka != "11"); // true
  print(notEqualAngka != "11"); // true
  print(notEqualAngka != 11);// false

  // Kurang dari & Lebih Dari ( <, >, <=, >=)
  var number = 17;
  print( number < 20 ); // true
  print( number > 17 ); // false
  print( number >= 17 ); // true, karena terdapat sama dengan
  print( number <= 20 ); // true

}



import 'dart:io';

void main(args) {

// input(args);
  // type();
  // operatorData();
  // number();
  dartKeyword();
}

void input(List<String> args) {
  print("masukan password");
  String inputText = stdin.readLineSync()!;
  print("password:  ${inputText}");
}

void type() {
//  type data 
  var name = "Misbahul Arifin";
  var angka = 22;
  var todayIsFriday = true;
  print(name);
  print(angka);
  print(todayIsFriday);
  
}

void operatorData() {
  // Equal Operator (==)
  var angka = 100;
  print(angka == 100); // true
  print(angka == 20); // false

  // Not Equal ( != )
  var sifat = "rajin";
  print(sifat != "malas"); // true
  print(sifat != "bandel"); //true 

  // Strict Equal ( == ) 
  var strickAngka = 8;
  print(strickAngka == "8"); // false
  print(strickAngka == "8"); // false
  print(strickAngka == 8); // true


  // Strict Not Equal ( != ) 
  var notEqualAngka = 11;
  print(notEqualAngka != "11"); // true
  print(notEqualAngka != "11"); // true
  print(notEqualAngka != 11);// false

  // Kurang dari & Lebih Dari ( <, >, <=, >=)
  var number = 17;
  print( number < 20 ); // true
  print( number > 17 ); // false
  print( number >= 17 ); // true, karena terdapat sama dengan
  print( number <= 20 ); // true

  // OR ( || )
  
  print(true || true); // true
  print(true || false); // true
  print(true || false || false); // true
  print(false || false); // false

  // AND ( && )
  print(true && true); // true
  print(true && false); // false
  print(false && false); // false
  print(false && true && true); // false
  print(true && true && true); // true
}

void number() {
  // declare an integer
   int num1 = 10;             
     
   // declare a double value
   double num2 = 10.50;  

   // print the values
   print(num1); //10
   print(num2); //10.5
   
   //mengubah string ke bentuk number
   print(num.parse('12')); //12
   print(num.parse('10.91')); //10.91
   //error mengubah string ke bentuk number
   print(num.parse('12A')); 
   print(num.parse('AAAA')); 
  
  // mengubah bentuk angka ke dalam bentuk string
  int j = 45;
   String t = "$j";  
   print("hello"+ t);
}

void dartKeyword() {
  final  umur = 21;
  const age = 22;
  
  final date = new DateTime.now();
  
  print(umur);
  print(age);
  print(date);
}


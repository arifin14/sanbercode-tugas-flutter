main(List<String> args) async {
  await messages(1, 'pernahkah kau merasa').then((value) => print(value));
  await messages(1, 'pernahkah kau merasa ....').then((value) => print(value));
  await messages(1, 'pernahkah kau merasa').then((value) => print(value));
  await messages(1, 'Hatiku hampa, pernah kah kau merasa hatimu kosong')
      .then((value) => print(value));
}

Future<String> messages(int seconds, String message) async {
  final duration = Duration(seconds: seconds);
  return await Future.delayed(duration).then((value) => message);
}

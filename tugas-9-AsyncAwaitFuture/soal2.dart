Future delayedPrint(int seconds, String message) {
  final duration = Duration(seconds: seconds);
  return Future.delayed(duration).then((value) => message);
}

main(List<String> args) {
  print("Quotes");
  delayedPrint(1, 'life').then((value) => print(value));
  delayedPrint(3, 'never flat').then((value) => print(value));
  delayedPrint(2, 'is').then((value) => print(value));
}


void main() {
  ifConditional();
  simpleBranch();
  multiConditional();
  nestedConditional();
  switchConditional();
}

// if else
void ifConditional() {
  var isFruitFree = true;

  if(isFruitFree){
      print("Silahkan diambil");
  }else{
      print("Lakukan pembayaran");
  }

  // short
  isFruitFree ? print("Silahkan diambil") : print("Lakukan pembayaran");
}

// simple branch conditional (if else)
void simpleBranch() {
  var minimarketStatus = "open";
  if (minimarketStatus == "open") {
    print("saya akan membeli Buah");
  } else {
    print("minimarketnya tutup");
  }
}
// 2 conditional (if else) 
void multiConditional() {
  var minimarketStatus = "close";
  var totalWatermelon = 5;
  if (minimarketStatus == "open") {
    print("saya akan membeli Semangka");
  } else if (totalWatermelon <= 5) {
    print("Saya akan membeli semangka sebagian ");
  } else {
    print("minimarket tutup, saya akan berbelanja esok hari");
  }
}

// nested conditional (if else)
void nestedConditional() {
  var minimarketStatus = "open";
  var cash = 100000;
  var watermelon = 90000;
  var watermelonStock = 10;
  if (minimarketStatus == "open") {
    print("saya akan membeli Semangka");
    if (cash  >= watermelon || watermelon <= cash) {
      print("Saya bisa membeli semngka");
    } else if (cash <= watermelon) {
      print("Uang tidak cukkup");
    } else if (watermelonStock <= 1) {
      print("buah habis");
    }
  } else {
    print("minimarket tutup, saya pulang lagi");
  }
}

// switch conditional 

void switchConditional() {
  var buttonPushed = 1;
  switch(buttonPushed) {
    case 1:   { print('matikan TV!'); break; }
    case 2:   { print('turunkan volume TV!'); break; }
    case 3:   { print('tingkatkan volume TV!'); break; }
    case 4:   { print('matikan suara TV!'); break; }
    default:  { print('Tidak terjadi apa-apa'); }}
}
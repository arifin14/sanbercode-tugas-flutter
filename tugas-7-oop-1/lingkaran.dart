import 'dart:math';

class Lingkaran{
  double phi = 3.14;
  late double _r;
  // late double _d;
  
  void setLuas(double value ) {
    if(value < 0) {
      value *= -1;
    }
    _r = value;
  }
  
  double getLuas() {
    return phi * pow(_r, 2);
  }

}
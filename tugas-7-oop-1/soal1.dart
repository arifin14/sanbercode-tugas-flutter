import 'lingkaran.dart';
void main() {
  
  // hitungLuasSegitiga();
  hitungLuasLingkaran();
}

// hitung luas segitiga
hitungLuasSegitiga() {
  LuasSegitiga segitiga;
  late double luasSegitiga;

  segitiga = new LuasSegitiga();
  
  segitiga.setengah = 0.5;
  segitiga.alas = 20.0;
  segitiga.tinggi = 30.0;

  luasSegitiga = segitiga.hitungLuas();

  print(luasSegitiga);
}


class LuasSegitiga {
  // double setengah, alas, tinggi;
  late double setengah;
  late double alas;
  late double tinggi;

  double hitungLuas() {
    return setengah * alas * tinggi;
  }

}

// hitung luas lingkaran
hitungLuasLingkaran() {
  Lingkaran lingkaran;
  double luas;

  lingkaran = new Lingkaran();
  lingkaran.setLuas(7);

  luas = lingkaran.getLuas();
  print(luas);

}



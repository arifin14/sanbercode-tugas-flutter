// import 'package:flutter/';
// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:sanberapp/tugas12/DrawerScreen.dart';
import 'chat_model.dart';

class TelegramScreen extends StatefulWidget {
  const TelegramScreen({Key? key}) : super(key: key);

  @override
  _TelegramScreenState createState() => _TelegramScreenState();
}

class _TelegramScreenState extends State<TelegramScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telegram"),
        actions: [
          Padding(
            padding: EdgeInsets.all(8),
            child: Icon(Icons.search),
          ),
        ],
        backgroundColor: Colors.cyan[600],
      ),
      drawer: DrawerScreen(),
      body: Center(
        child: ListView.separated(
            itemBuilder: (ctx, i) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(items[i].profileUrl),
                ),
                title: Text(items[i].name),
                subtitle: Text(items[i].message),
                trailing: Text(items[i].time),
              );
            },
            separatorBuilder: (ctx, i) {
              return Divider();
            },
            itemCount: items.length),
      ),
    );
  }
}

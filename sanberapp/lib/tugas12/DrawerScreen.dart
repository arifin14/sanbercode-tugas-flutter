import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Misbahul Arifin"),
            accountEmail: Text("misbahularifin14@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/images/photo.jpg"),
            ),
            decoration: BoxDecoration(color: Colors.cyan[700]),
          ),
          DrawerList(
            onPress: () {},
            title: "New Group",
            iconData: Icons.group,
          ),
          DrawerList(
            onPress: () {},
            title: "New Secret Group",
            iconData: Icons.lock,
          ),
          DrawerList(
            onPress: () {},
            title: "New Chanel Chat",
            iconData: Icons.notifications,
          ),
          DrawerList(
            onPress: () {},
            title: "Contact",
            iconData: Icons.contacts,
          ),
          DrawerList(
            onPress: () {},
            title: "Saved Messages",
            iconData: Icons.bookmark_border,
          ),
          DrawerList(
            onPress: () {},
            title: "Calls",
            iconData: Icons.phone,
          ),
        ],
      ),
    );
  }
}

class DrawerList extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onPress;

  const DrawerList({
    Key? key,
    required this.iconData,
    required this.title,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onPress,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

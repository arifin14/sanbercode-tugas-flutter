import 'package:flutter/material.dart';
import 'package:sanberapp/tugas12/DrawerScreen.dart';

class UserHomeScreen extends StatefulWidget {
  const UserHomeScreen({Key? key}) : super(key: key);

  @override
  _UserHomeScreenState createState() => _UserHomeScreenState();
}

class _UserHomeScreenState extends State<UserHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Welcome',
        ),
        backgroundColor: Colors.cyan[600],
        elevation: 0,
      ),
      body: Center(
        child: Text("user home"),
      ),
      drawer: DrawerScreen(),
    );
  }
}

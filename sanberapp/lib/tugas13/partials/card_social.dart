import 'package:flutter/material.dart';

class CardSocial extends StatelessWidget {
  final String logo;
  final String username;
  final String label1;
  final String label2;
  final String label3;
  final String val1;
  final String val2;
  final String val3;

  const CardSocial({
    Key? key,
    required this.logo,
    required this.username,
    required this.label1,
    required this.label2,
    required this.label3,
    required this.val1,
    required this.val2,
    required this.val3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: SizedBox(
        width: double.infinity,
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Image(
                height: 50,
                image: AssetImage(logo),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 200,
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                    username,
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                    width: 200,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          child: Column(children: [
                            Text(
                              val1,
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              label1,
                              style: TextStyle(fontSize: 12),
                            )
                          ]),
                        ),
                        SizedBox(
                          child: Column(children: [
                            Text(
                              val2,
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              label2,
                              style: TextStyle(fontSize: 12),
                            )
                          ]),
                        ),
                        SizedBox(
                          child: Column(children: [
                            Text(
                              val3,
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              label3,
                              style: TextStyle(fontSize: 12),
                            )
                          ]),
                        )
                      ],
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}

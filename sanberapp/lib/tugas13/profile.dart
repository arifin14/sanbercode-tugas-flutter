import 'package:flutter/material.dart';
import 'package:sanberapp/tugas13/login.dart';
import 'package:sanberapp/tugas13/partials/card_social.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen())),
                      child: Text(
                        "Sign Out",
                        style: TextStyle(color: Colors.grey[700]),
                      )),
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        "Edit profile",
                        style: TextStyle(color: Colors.grey[700]),
                      )),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              width: double.infinity,
              height: 300,
              child: Stack(
                children: [
                  Container(
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(5)),
                    child: Image(
                      image: AssetImage('assets/images/banner.png'),
                    ),
                  ),
                  Positioned(
                      bottom: -5,
                      left: 10,
                      child: Container(
                        child: Image(
                          image: AssetImage('assets/images/photo_profile.png'),
                        ),
                      )),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Misbahul Arifin',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'UI Designer, Mobile Dev',
                    style: TextStyle(fontSize: 14, color: Colors.grey[600]),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            width: double.infinity,
            height: 150,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  CardSocial(
                      logo: 'assets/images/icon/instagram.png',
                      username: '_msbarf',
                      label1: 'Posts',
                      label2: 'Follower',
                      label3: 'Following',
                      val1: '100',
                      val2: '314',
                      val3: '310'),
                  SizedBox(
                    width: 10,
                  ),
                  CardSocial(
                      logo: 'assets/images/icon/github.png',
                      username: '_msbarf',
                      label1: 'Repositories',
                      label2: 'Following',
                      label3: 'Follower',
                      val1: '5',
                      val2: '1',
                      val3: '1'),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              height: 300,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  )),
              child: ListView(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      'Skills Info',
                      style: TextStyle(color: Colors.grey[600]),
                    ),
                  ),
                  Container(
                    color: Colors.grey[100],
                    child: ListTile(
                      leading: SizedBox(
                        height: 30,
                        child: Image(
                          image: AssetImage('assets/images/icon/vue.png'),
                        ),
                      ),
                      title: Text('Vue js'),
                      trailing: SizedBox(
                          width: 200,
                          child: LinearProgressIndicator(
                            value: 0.78,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.green),
                            backgroundColor: Colors.grey[900],
                            minHeight: 10,
                          )),
                    ),
                  ),
                  Container(
                    color: Colors.grey[50],
                    child: ListTile(
                      leading: SizedBox(
                        height: 30,
                        child: Image(
                          image: AssetImage('assets/images/icon/laravel.png'),
                        ),
                      ),
                      title: Text('Vue js'),
                      trailing: SizedBox(
                          width: 200,
                          child: LinearProgressIndicator(
                            value: 0.78,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.red),
                            backgroundColor: Colors.grey[900],
                            minHeight: 10,
                          )),
                    ),
                  ),
                  Container(
                    color: Colors.grey[100],
                    child: ListTile(
                      leading: FlutterLogo(),
                      title: Text('Vue js'),
                      trailing: SizedBox(
                          width: 200,
                          child: LinearProgressIndicator(
                            value: 0.78,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.blue),
                            backgroundColor: Colors.grey[900],
                            minHeight: 10,
                          )),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

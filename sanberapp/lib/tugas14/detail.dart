import 'package:flutter/material.dart';

class DetailNewsScreen extends StatefulWidget {
  List? value;
  DetailNewsScreen({
    Key? key,
    required this.value,
  }) : super(key: key);

  @override
  _DetailNewsScreenState createState() => _DetailNewsScreenState(this.value);
}

class _DetailNewsScreenState extends State<DetailNewsScreen> {
  List? value;
  _DetailNewsScreenState(this.value);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(value![0]),
      ),
      body: Center(
        child: Text(value![1]),
      ),
    );
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanberapp/tugas14/detail.dart';
import 'package:sanberapp/tugas14/post.dart';

class RestScreen extends StatefulWidget {
  const RestScreen({Key? key}) : super(key: key);

  @override
  _RestScreenState createState() => _RestScreenState();
}

class _RestScreenState extends State<RestScreen> {
  final String url = 'https://achmadhilmy-sanbercode.my.id/api/v1/';
  List? news;

  @override
  void initState() {
    super.initState();
    _getRefresh();
  }

  Future<void> _getRefresh() async {
    this.getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http
        .get(Uri.parse(url + 'news'), headers: {"Accept": "application/json"});
    print(response.body);
    var convert = jsonDecode(response.body);
    setState(() {
      news = convert["data"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: RefreshIndicator(
        onRefresh: _getRefresh,
        child: news!.length > 0
            ? ListView.builder(
                itemCount: news!.length,
                itemBuilder: (BuildContext contex, int index) {
                  return ListTile(
                      title: Text(news![index]['title']),
                      subtitle: Text(news![index]['value']),
                      trailing: Icon(Icons.chevron_right_outlined),
                      onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (contex) => DetailNewsScreen(
                                      value: [
                                        news![index]["title"],
                                        news![index]["value"]
                                      ],
                                    )),
                          ));
                },
              )
            : Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("News Empty"),
                  TextButton(onPressed: _getRefresh, child: Text("try again"))
                ],
              )),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => PostNewsSreen())),
        child: Icon(Icons.add),
      ),
    );
  }
}

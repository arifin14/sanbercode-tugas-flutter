import 'package:flutter/material.dart';
// import 'package:sanberapp/tugas14/main.dart';
import 'package:sanberapp/tugas14/model/user_model.dart';
import 'package:http/http.dart' as http;

class PostNewsSreen extends StatefulWidget {
  const PostNewsSreen({Key? key}) : super(key: key);
  @override
  _PostNewsSreenState createState() => _PostNewsSreenState();
}

class DialogProgres {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
            onWillPop: () async => false,
            child: SimpleDialog(
              backgroundColor: Colors.black54,
              children: [
                Center(
                  child: Column(
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Please wait...",
                        style: TextStyle(color: Colors.lightBlue),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}

class _PostNewsSreenState extends State<PostNewsSreen> {
  UserModel? userModel;

  final TextEditingController titleController = TextEditingController();
  final TextEditingController valueController = TextEditingController();

  void _submited(BuildContext context) async {
    final String title = titleController.text;
    final String value = valueController.text;

    // await createUser(title, value);
    final UserModel? _user = await createUser(title, value);
    setState(() {
      userModel = _user;
    });

    Navigator.pop(context);
  }

  Future createUser(String title, String value) async {
    try {
      // show dialog progress
      DialogProgres.showLoadingDialog(context, _keyLoader);
      var apiUrl =
          Uri.parse("https://achmadhilmy-sanbercode.my.id/api/v1/news");
      final response =
          await http.post(apiUrl, body: {"title": title, "value": value});
      if (response.statusCode == 201) {
        print(response.body);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add News"),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: "News Title"),
            ),
            SizedBox(
              height: 30,
            ),
            TextField(
              controller: valueController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: "Value"),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _submited(context),
        child: Icon(Icons.save),
      ),
    );
  }
}

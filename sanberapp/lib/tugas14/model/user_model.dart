import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));
String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    required this.id,
    required this.title,
    required this.value,
    required this.createdAt,
  });

  String id;
  String title;
  String value;
  DateTime createdAt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
      id: json["id"],
      title: json["title"],
      value: json["value"],
      createdAt: DateTime.parse(json["createdAt"]));

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'value': value,
        'createdAt': createdAt.toIso8601String(),
      };
}

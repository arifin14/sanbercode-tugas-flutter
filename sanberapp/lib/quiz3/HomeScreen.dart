import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                IconButton(icon: Icon(Icons.extension), onPressed: () {})
              ],
            ),
            SizedBox(height: 20),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Welcome, \n",
                    style: TextStyle(color: Colors.blue[300]),
                  ),
                  TextSpan(
                    text: "Hilmy, \n",
                    style: TextStyle(color: Colors.blue[900]),
                  ),
                ],
              ),
              style: TextStyle(fontSize: 30),
            ),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, size: 18),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: "Search"),
            ),
            SizedBox(height: 10),
            Text(
              "Recomended Place",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            // <!-- //? #Soal No 2 (20 poin) -- HomeScreen.js -- Function HomeScreen
            //? Buatlah 1 komponen GridView dengan input berasal dari assets/img yang sudah disediakan

            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)
            //? untuk tampilan apabila ada warning boleh diabaikan asal data gambar tampil
            // -->
            // tuliskan coding disini
            SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 270,
              width: double.infinity,
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 360,
                      childAspectRatio: 4 / 3,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10),
                  itemCount: countries.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return Container(
                      child: Column(
                        children: [
                          Image.asset(countries[index]["image"]),
                          SizedBox(
                            height: 5,
                          ),
                          Text(countries[index]["country"])
                        ],
                      ),
                    );
                  }),
            ),
            // end coding
          ],
        ),
      ),
    );
  }
}

final List countries = [
  {"country": "Tokyo", "image": "assets/quiz/Tokyo.png"},
  {"country": "Berlin", "image": "assets/quiz/Berlin.png"},
  {"country": "Roma", "image": "assets/quiz/Roma.png"},
  {"country": "Monas", "image": "assets/quiz/Monas.png"},
  {"country": "London", "image": "assets/quiz/London.png"},
  {"country": "Paris", "image": "assets/quiz/Paris.png"},
];

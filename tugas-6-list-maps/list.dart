void main() {
  print(range(1, 10));
  print(rangeWithStep(1, 10, 2));
  dataHandling();
  print(balikKata('sayur'));
}
// soal nomer 1
range(int startNum, int finishNum) {
  return List<int>.generate(finishNum, (i) => i + startNum);
}
// soal nomer 2
rangeWithStep( int startNum ,int finishNum, int step) {
  if (step == 0)
    throw Exception("Nilai step tidak boleh 0");

  return startNum < finishNum == step > 0
  ? List<int>.generate(((startNum-finishNum)/step).abs().ceil(), (int i) => startNum + (i * step))
  : [];
}
//soal nomer 3
dataHandling() {
  var inputData= [
      ["0001", "Romlan Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
      ["0002", "Dika Simbring", "Medan", "10/10/1992", "Bermain Gitar"],
      ["0003", "Winona", "Ambon", "25/12/1996", "Memasak"],
      ["0004", "Bintang Senjaya", "Martapura", "06/04/1970", "Berkebun"],
  ];

  inputData.forEach((index) => {
    print('Nomer ID : ${index[0]} \nNama Lengkap : ${index[1]} \nTTL : ${index[2]} ${index[3]} \nHobi : ${index[4]} \n\n ')
  });
}
// soal nomer 4 Balik kata
balikKata(String kata) {
  return kata.split('').reversed.join();
}